branches=()
eval "$(git for-each-ref --shell --format='branches+=(%(refname))' refs/remotes)"

count=0
file="ListActiveSimBranch.txt"
for branch in "${branches[@]}"; do
	checkBranch="NoMatch"

	while IFS= read line
	do
		if [[ ! -z "$line" && "%$line: =%"!=*' '* ]]; then
			currBranch="refs/remotes/origin/$line"
		    # display $line or do somthing with $line
			if [[ "$branch" == *"$(echo "$currBranch")"* ]]; then
			 	checkBranch="Match"
			fi
			branch2=${branch:20}
		fi
	done <"$file"

    if [ "$checkBranch" == "Match" ]; then
    	git push origin :"$branch2"
    	echo "$branch2 has just been deleted"
    	count= $count + 1
    fi
done

if [[ $count < 1 ]]; then
	echo "No branch has deteled"
	echo "Job is finished"
fi
